# ejohn-classjs
## Introduction:

See http://ejohn.org/blog/simple-javascript-inheritance/

## Usage:
### Access Via Server:
```js
(function main( Class ){
    var SuperClass = Class.extend({
	init : function(){
	    //set some variables
	    this.setString('Hello World');
	},
	getString : function(){
	    //get variable
	    return this.class_string;
	},
	setString : function( new_value ){
	    this.class_string = new_value;
	}
    }),
    SubClass = SuperClass.extend({
	init : function( prefix ){
	    this.prefix = prefix;
	    //access super class
	    this._super();
	    
	},
	setString : function( new_value ){
	    //override
	    this._super( this.prefix + new_value );
	}
    }),
    super_obj = new SuperClass(),
    sub_obj = new SubClass( 'sub::' );
    console.log('super::',super_obj.getString());
    console.log('sub::',sub_obj.getString());
})( 
    require('ejohn-classjs')
);  
```
### Access Via Client:
```js
(function main( document ){
  require( 'ejohn-classjs', function( Class ){
    var SuperClass = Class.extend(
      init : function(){
        //set some variables
        this.setString('Hello World');
      },
      getString : function(){
        //get variable
        return this.class_string;
      },
      setString : function( new_value ){
        this.class_string = new_value;
      }
    ),
    SubClass = SuperClass.extend(
      init : function( prefix ){
        this.prefix = prefix;
        //access super class
        this._super();
        
      },
      setString : function( new_value ){
        //override
        this._super( this.prefix + new_value );
      }
    ),
    super_obj = new SuperClass(),
    sub_obj = new SubClass( 'sub::' );
  });
})( document );  
```
### Create Class to be Accessed Both:
```js
(function main( ){
  var SuperClass = Class.extend(
    init : function(){
      //set some variables
      this.setString('Hello World');
    },
    getString : function(){
      //get variable
      return this.class_string;
    },
    setString : function( new_value ){
      this.class_string = new_value;
    }
  ),
  SubClass = SuperClass.extend(
    init : function( prefix ){
      this.prefix = prefix;
      //access super class
      this._super();
      
    },
    setString : function( new_value ){
      //override
      this._super( this.prefix + new_value );
    }
  )
  ;
  if( typeof exports === 'undefined' ){
    return {
      SuperClass : SuperClass,
      SubClass : SubClass
    }    
  }else{
    exports.SuperClass = SuperClass;
    exports.SubClass = SubClass;
  }
})( );  
```
