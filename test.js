(function test( Class ){
    var SuperClass = Class.extend({
	init : function(){
	    //set some variables
	    this.setString('Hello World');
	},
	getString : function(){
	    //get variable
	    return this.class_string;
	},
	setString : function( new_value ){
	    this.class_string = new_value;
	}
    }),
    SubClass = SuperClass.extend({
	init : function( prefix ){
	    this.prefix = prefix;
	    //access super class
	    this._super();
	    
	},
	setString : function( new_value ){
	    //override
	    this._super( this.prefix + new_value );
	}
    }),
    super_obj = new SuperClass(),
    sub_obj = new SubClass( 'sub::' );
    console.log('super::',super_obj.getString());
    console.log('sub::',sub_obj.getString());
})( require('./ejohn-classjs') );
